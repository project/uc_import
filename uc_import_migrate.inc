<?php
// vim: set tabstop=2 sw=2:

class ProdottiPadreMigration extends Migration {
  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('Prodotti'));
    $this->description = t('Importa i prodotti dalla tabella di supporto dopo l\'import del csv');
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'id' => array(
          'type'        => 'int',
          'unsigned'    => TRUE,
          'not null'    => TRUE,
          'description' => 'id csv support table',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    $query = db_select('uc_import_csv_data', 'csv')->fields('csv')
		->condition(
			db_or()
				->isNull('csv.parent_sku')
				->condition('csv.parent_sku', '', '=')
		);
    $source_field = array (
      'supplier'   => 'aka Brand',
      'id'         => 'Primary key, auto increment',
      'sku'        => 'il sotto sku del prodotto, i sotto prodotti hanno questo come parent_sku',
      'parent_sku' => 'riempito solo per le righe che sono dei sotto prodotti',
      'attributes' => 'attributi',
      'body'       => 'body in ita, preparato in prepareRow',
    );
    $this->source = new MigrateSourceSQL($query, $source_field, NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationNode('product');
    $this->addFieldMapping('title', 'title_it');
    $this->addFieldMapping('status', 'published');
    $this->addFieldMapping('promote', 'promoted');
    $this->addFieldMapping('list_price', 'list_price');
    $this->addFieldMapping('cost', 'cost');
    $this->addFieldMapping('sell_price', 'sell_price');
    $this->addFieldMapping('weight', 'weight');
    $this->addFieldMapping('length', 'length');
    $this->addFieldMapping('width', 'width');
    $this->addFieldMapping('height', 'height');
    $this->addFieldMapping('default_qty', 'default_quantity');
    $this->addFieldMapping('pkg_qty', 'package_quantity');

    $arguments = MigrateFileFieldHandler::arguments(NULL, 'file_copy', FILE_EXISTS_REPLACE, NULL, '', '');

    $defined_columns = _uc_import_get_columns_from_table();
    $custom_fields = content_fields(NULL, 'product');
    foreach ($custom_fields as $cf) {
      $cf_field_name = $cf['field_name'];
	  // FILE_FIELD
      if ($cf['type'] == 'filefield') {
        $this->addFieldMapping($cf_field_name, $cf_field_name)
          ->arguments($arguments)
          ->description('The mapping on the real field is done in prepareRow()');
        $this->filefield_fieldnames_and_cardinality[$cf_field_name] =  0;
        foreach ($defined_columns as $colname) {
          if (substr($colname, 0, strlen($cf_field_name)) == $cf_field_name && substr($colname, -5) == '_path') {
            $int_found = (int)substr($colname, strlen($cf_field_name)+1, -5);
            if ($int_found > $this->filefield_fieldnames_and_cardinality[$cf_field_name]) {
              $this->filefield_fieldnames_and_cardinality[$cf_field_name] = $int_found;
            }
          }
        }
      }
	  // OTHER 'SCALAR' FIELDS
      else if (in_array($cf['type'], array('text', 'number_decimal', 'number_integer',  'nodereference'))) {
        $this->addFieldMapping($cf_field_name, $cf_field_name)->callbacks('stripslashes');
      }
    }
  }

  public function preImport() {
    parent::preImport();
  }

  public function prepareRow(&$current_row) {
    $history_path = file_create_path(UCIMPORT_HISTORY);
    $import_identifier = $current_row->import_id;
    $destination_path1 = $history_path."/$import_identifier";
    $destination_path2 = $history_path."/$import_identifier"."/files/";
    if (
      !file_check_directory(file_create_path($destination_path1), FILE_CREATE_DIRECTORY) ||
      !file_check_directory(file_create_path($destination_path2), FILE_CREATE_DIRECTORY)) {
      drupal_set_message('Errore di sistema, non e\' possibile creare i percorsi per la history');
      return FALSE;
    }

	// PARSING FILES AND COPYING TO HISTORY DIRECTORY
    foreach ($this->filefield_fieldnames_and_cardinality as $fieldname => $cardinality_in_table) {
      for ($i = 1; $i <= $cardinality_in_table; $i++) {
        $column_name = $fieldname . '_' . $i . '_path';
        if (!empty($current_row->$column_name)) {
          $trimmed_filename_from_csv = ltrim($current_row->$column_name, "/\\");
          $path_under_ftp_area = $current_row->subdir_for_files . "/" . $trimmed_filename_from_csv;
          $full_path = rtrim(file_create_path(UCIMPORT_DATA_SUBDIR), "/\\") . "/" . $path_under_ftp_area;
          $current_row->{$fieldname}[] = $full_path;

          // now copy the file from the ftp area into the zip file
          $zipfile = new ZipArchive();
          if ($zipfile->open($destination_path2.$import_identifier.'.zip', ZIPARCHIVE::CREATE) !== TRUE) {
            return FALSE;
          }
          $zipfile->addFile($full_path, $path_under_ftp_area);
          $zipfile->close();
        }
      }
    }
  }

  public function prepare (&$entity, $current_row) {
    global $user;
    $bodies['en'] = $current_row->body_en;
    $bodies['it'] = $current_row->body_it;
    if (!empty($bodies['en'])) {
      $current_row->body = $bodies['en'];
    } else if (!empty($bodies['it'])) {
      $current_row->body = $bodies['it'];
    }
    $entity->body = $current_row->body;
    $entity->model = $current_row->sku;
    $entity->uid = $user->uid;

    // deal with the vocabulary
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $vocabulary) {
      $vid = $vocabulary->vid;
      $column = 'vocabulary_' . $vid;
      $terms = $current_row->$column;
      if (empty($terms)) {
        continue;
      }
      $terms_array = explode('|', $terms);
      $terms_array = array_map('trim', $terms_array);
      foreach ($terms_array as $csv_term) {
        $t = array (
          'name' => $csv_term,
          'vid'  => $vid,
        );
        $existing_terms = taxonomy_get_term_by_name($csv_term);
        if (empty($existing_terms)) {
          taxonomy_save_term($t);
          $entity->taxonomy[$t['tid']] = (object)$t;
        } else {
          foreach ($existing_terms as $existing_term) {
            if ($existing_term->vid == $vid) {
              $t['tid'] = $existing_term->tid;
              $t['weight'] = $existing_term->weight;
              $parents_obj = taxonomy_get_parents($t['tid']);
              $parents = array();
              foreach ($parents_obj as $pid => $p) {
                $parents[] = $pid;
              }
              $t['parent'] = $parents;
              taxonomy_save_term($t);
              $entity->taxonomy[$t['tid']] = (object)$t;
            }
          }
        }
      } // foreach ($terms_array as $csv_term) {
    } // foreach ($vocabularies as $vocabulary)

    foreach ($this->filefield_fieldnames_and_cardinality as $fieldname => $cardinality_in_table) {
      for ($i = 1; $i <= $cardinality_in_table; $i++) {
        $column_name_description = $fieldname . '_' . $i . '_description';
        $column_name_title       = $fieldname . '_' . $i . '_title';
        // if we are updating a node, and one of the images in the csv table does not exist
        // load the original node and use its images
        if (isset($entity->nid) && !file_exists($current_row->{$fieldname}[$i-1])) {
          $node_original = node_load($entity->nid);
          if (!empty($node_original->{$fieldname}[$i-1])) {
            $entity->{$fieldname}[] = $node_original->{$fieldname}[$i-1];
          }
        }

        if (!empty($current_row->$column_name_description)) {
          $entity->{$fieldname}[$i-1]['data']['description'] = $current_row->$column_name_description;
          $entity->{$fieldname}[$i-1]['data']['alt'] = $entity->{$fieldname}[$i-1]['data']['description'];
        }
        if (!empty($current_row->$column_name_title)) {
          $entity->{$fieldname}[$i-1]['data']['title'] = $current_row->$column_name_title;
        }
      }
    }
  }

  public function complete(stdClass $node, stdClass $row) {
    $this->makeSureStoreOptionExists($row);
    $this->associateAttributesOptionsAndAlternateSkus($node, $row);
    $this->updateStock($node, $row);
  }

  private function makeSureStoreOptionExists(stdClass $row) {
    $query = db_select('uc_import_csv_data', 'csv')
      ->fields('csv')
      ->condition('csv.supplier',   $row->supplier, '=')
      ->condition('csv.parent_sku', $row->sku, '=');
    $result = $query->execute();
    $store_attributes = uc_attribute_load_multiple();
    while ($subproduct = $result->fetchAssoc()) {
      foreach ($store_attributes as $aid => $default_attr_and_options) {
        $attr_name = _uc_import_get_csv_name_for($default_attr_and_options->name, 'attribute');
        if (!empty($subproduct[$attr_name])) {
          $do_insert_default_store_option_for_default_attr = TRUE;
          foreach ($default_attr_and_options->options as $oid => $option) {
            if ($option->name == $subproduct[$attr_name]) {
              $do_insert_default_store_option_for_default_attr = FALSE;
            }
          }
          if ($do_insert_default_store_option_for_default_attr) {
            db_query("INSERT INTO {uc_attribute_options} (aid, name, cost, price, weight, ordering) VALUES (%d, '%s', %f, %f, %f, %d)", $aid, $subproduct[$attr_name], 0,0,0,0);
            $std = new stdClass();
            $std->name = $subproduct[$attr_name];
            $default_attr_and_options->options[] = $std;
          }
        }
      }
    }
  }

  private function associateAttributesOptionsAndAlternateSkus(stdClass $node, stdClass $row) {
    $query = db_select('uc_import_csv_data', 'csv')
      ->fields('csv')
      ->condition('csv.supplier',   $row->supplier, '=')
      ->condition('csv.parent_sku', $row->sku, '=');
    $result = $query->execute();
    $store_attributes = uc_attribute_load_multiple();
    while ($subproduct = $result->fetchAssoc()) {
      $attribute_option_attivi = array();
      // Passa in rassegna per ogni subproduct tutti gli attribute di default.
      foreach ($store_attributes as $aid => $default_attr_and_options) {
        $attr_name = _uc_import_get_csv_name_for($default_attr_and_options->name, 'attribute');
        // Se il subproduct ha l'attributo valorizzato.
        if (!empty($subproduct[$attr_name])) {
          // Cerca nel $default_attr_and_options->options quale e' l'opzione che corrisponde
          // a questa riga di subproduct.
          // L'ultimo subproduct incontrato vince nel senso che e' il default.
          foreach ($default_attr_and_options->options as $oid => $option) {
            if ($option->name == $subproduct[$attr_name]) {
              // specifica che il nid HA tale attributo (con default option quella in esame).
              $merge_query = db_merge('uc_product_attributes')
                ->key(array(
                  'nid' => (int)$node->nid,
                  'aid' => (int)$aid,
                ))
                ->fields(array(
                  'label'          => $default_attr_and_options->name,
                  'ordering'       => 0,
                  'default_option' => $oid,
                  'required'       => 0,
                  'display'        => 1,
                ));
                //->updateFields(array());
              $merge_query->execute();

              // specifica che tale nid ha attivata tale option.
              $merge_query = db_merge('uc_product_options')
                ->key(array(
                  'nid' => (int)$node->nid,
                  'oid' => (int)$oid,
                ))
                ->fields(array(
                  'cost'     => 0,
                  'price'    => 0,
                  'weight'   => 0,
                  'ordering' => 0,
                ));
              $merge_query->execute();

              $attribute_option_attivi[$aid] = (string)$oid;
            }
          }
        }
      }
      // definisci gli alternate sku di questo nodo in base a che attributi*options sono attivi su questo subproduct
      db_query("INSERT into uc_product_adjustments (nid, combination, model) VALUES (%d, '%s', '%s')", $node->nid, serialize($attribute_option_attivi), $subproduct['sku']);
    }
  }

  private function updateStock(stdClass $node, stdClass $row) {
    if (!empty($row->stock)) {
      $this->_uc_import_upsert_stock($node->nid, $node->model, $row->stock);
    }

    $query = db_select('uc_import_csv_data', 'csv')
      ->fields('csv')
      ->condition('csv.supplier',   $row->supplier, '=')
      ->condition('csv.parent_sku', $row->sku, '=');
    $result = $query->execute();
    while ($subproduct = $result->fetchAssoc()) {
      if (!empty($subproduct['stock'])) {
        $this->_uc_import_upsert_stock($node->nid, $subproduct['sku'], $subproduct['stock']);
      }
    }
  }

  private function _uc_import_upsert_stock ($nid, $model, $stock) {
    //error_log(print_r('nid='.$nid. ' model='.$model. ' stock='.$stock, TRUE));
    db_query("UPDATE {uc_product_stock} SET active = %d, stock = %d, threshold = %d WHERE nid = %d and sku = '%s'",
      1, intval($stock), 0, $nid, $model);

    if (!db_affected_rows()) {
      db_query("INSERT INTO {uc_product_stock} (sku, nid, active, stock, threshold) VALUES ('%s', %d, %d, %d, %d)",
        $model, $nid, 1, intval($stock), 0);
    }

  }
}
