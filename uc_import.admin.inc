<?php

function uc_import_csv_import_form (&$form_state) {
  global $user;
  
  
  $module_path = drupal_get_path('module', 'uc_import');
  drupal_add_js($module_path . '/uc_import.js');
  drupal_add_css($module_path . '/uc_import.css');
  
  if (
    !file_check_directory(file_create_path(UCIMPORT_ROOT_DIR), FILE_CREATE_DIRECTORY) ||
    !file_check_directory(file_create_path(UCIMPORT_DATA_SUBDIR), FILE_CREATE_DIRECTORY) ||
    !file_check_directory(file_create_path(UCIMPORT_HISTORY), FILE_CREATE_DIRECTORY)
  ) {
    drupal_set_message('Failed to create the directories, please check the permissions', 'error');
    return;
  }
  $form = array(
    '#attributes' => array('enctype' => "multipart/form-data"),
  );
  $form['file_csv'] = array (
    '#type'     => 'file',
    '#title'    => '.CSV FILE',
    '#required' => TRUE,
    '#value'    => 'file_csv',
    '#description' => 'Please upload only a kindly formatted .csv, with ; as separator. <a href="#" id="example-csv-link" class="not-implemented">Click here</a> to download an example',
 
  );
  
  $form['import_files'] = array(

    '#type' =>'checkbox', 
	'#title' => t('Import Files'),
	'#description' => t('Import files from specified direrctory.'),
	'#default_value' => TRUE,

  );

  
  $form['subdir_uploaded_files'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Directory'),
    '#required'    => TRUE,
    '#description' => 'Specify directory UNDER '.file_directory_path().'/'.UCIMPORT_DATA_SUBDIR.' where all files relarted to import have been uploaded',
    //'#default_value' => 'files',
  );
  
  $form['uc_import_types'] = array(
	  '#type' => 'checkboxes', 
	  '#title' => t('Import types'), 
	  '#default_value' => variable_get('uc_import_types', 
	  	array(
			'texts', 
			'attributes',
			'taxonomy',
			'stock',
		)
	  ),
	  '#options' => array(
		  'texts' => t('Main Texts (title, body)'), 
		  'attributes' => t('Attributes'), 
		  'taxonomy' => t('Categories (Taxonomy terms)'),
		  'stock' => t('Stock values'),
	  ),
	  '#disabled' => TRUE,
	 '#description' => 'Select which type of data are imported or not',
  );
  
  $form['start_migration'] = array (
    '#type'     => 'checkbox',
    '#title'    => t('Start migration'),
    '#default_value' => FALSE,
	'#description' => "Start migration just after the file upload is complete. Please remind that it can takes a lot of time, so it's not a good option if you are uploading a series of .csv files",
	
  );
  
  $form['fieldset_optional_fields'] = array (
    '#type' => 'fieldset',
    '#title' => t('Optional fields'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['fieldset_optional_fields']['import_name'] = array (
    '#type'        => 'textfield',
    '#title'       => t('Import name'),
    '#required'    => FALSE,
    '#maxlength'   => 125,
    //'#size'   => 10,
    '#description' => 'This field allows yout to specify a custom handle name for this import, mostly for debug or history purposes',
  );
  
  
  


  $form['submit'] = array (
    '#type'  => 'submit',
    '#value' => t('Carica'),
  );
  return $form;
}

function uc_import_csv_import_form_validate ($form, &$form_state) {
  $file = file_save_upload('file_csv', array(
    'file_validate_extensions' => array('csv'),
  ));
  // If the file passed validation:
  if (isset($file->filename)) {
    $history_path = file_create_path(UCIMPORT_HISTORY);
    $import_identifier = _uc_import_get_import_id($form_state['values']['import_name']);
    $destination_path1 = $history_path."/$import_identifier";
	
	$files_dir = file_create_path(UCIMPORT_DATA_SUBDIR)."/".$form_state['values']['subdir_uploaded_files'];
    if (!file_check_directory($files_dir,FILE_CREATE_DIRECTORY) && $form_state['values']['import_files'] == TRUE) {
      form_set_error('file_csv', t('Directory "'.$files_dir.'" not found in data dir: are you sure you uploaded or created it?'));
    }
	//uc_import_debug($form_state['values']);
    if (!file_check_directory(file_create_path($destination_path1), FILE_CREATE_DIRECTORY)) {
      form_set_error('file_csv', t('Cannot create directory/files for history'));
    }
    if (file_move($file, $destination_path1 . "/import.csv")) {
      $form_state['storage']['file_csv'] = $file;
    }
  }
  else {
    form_set_error('file_csv', t('Invalid file, only .csv files are allowed'));
  }
}

function uc_import_csv_import_form_submit ($form, &$form_state) {
  global $user;
  $file = $form_state['storage']['file_csv'];
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['file_csv']);

  $import_identifier = _uc_import_get_import_id($form_state['values']['import_name']);
  $insert_query = db_insert('uc_import_csv_submission_history')
    ->fields(array('import_identifier', 'uid', 'date'))
    ->values(array(
    'import_identifier' => $import_identifier,
    'uid'               => $user->uid,
    'date'              => date('Y-m-d h:i:s'),
  ));
  $new_aid = $insert_query->execute();

  $returned = array();
  $ret = _uc_import_parse_csv(array(
    'file'                  => $file,
    'import_id'             => $import_identifier,
    'subdir_uploaded_files' => $form_state['values']['subdir_uploaded_files'],
  ), $returned);

  $log_file_path = rtrim(file_create_path(UCIMPORT_ROOT_DIR), "/\\") . "/" .UCIMPORT_REPORT_FILENAME;
  $message = '<a target="_blank" href="' . url($log_file_path,array('query' => array('r' => rand()))) .'">Download report</a>';
  if (count($returned['num_errors'])) {
    $message .= " (".$returned['num_errors'].")" . " Errore/i";
    drupal_set_message($message, 'error');
  } else {
    drupal_set_message($message, 'status');
  }

  if ($ret == FALSE) {
    drupal_set_message(t('An error has occurred importing the file'), 'error');
    return;
  }
  drupal_set_message(t('The form has been submitted and the csv has been imported.'));
  if ($form_state['values']['start_migration']) {
    $migration = Migration::getInstance('ProdottiPadre');
    $migration->prepareUpdate();
    $result = $migration->processImport();
  }
}

function uc_import_csv_map (&$form_state) {
    $module_path = drupal_get_path('module', 'uc_import');
    drupal_add_js($module_path . '/uc_import.js');
    drupal_add_css($module_path . '/uc_import.css');

  
  $all_store_attributes = uc_attribute_load_multiple();
  $form['fieldset_attributes'] = array (
    '#type' => 'fieldset',
    '#title' => 'Associazione attributes - colonne csv',
    '#description' => 'Gli store attributes devono esistere gia\'. La procedura di migrate aggiunge gli store options allo specifico store attribute se non c\'e\' gia\'',
  );
  foreach ($all_store_attributes as $attr) {
    $form['fieldset_attributes']['attr_'.$attr->aid] = array(
      '#type'          => 'textfield',
      '#title'         => 'Colonna nel csv per '.$attr->name,
      '#multiple'      => FALSE,
      '#disabled'      => TRUE,
      '#value'         => _uc_import_get_csv_name_for($attr->name, 'attribute'),
      //'#default_value' => $default_values[$voc->vid],
    );
  }

  $vocabularies = taxonomy_get_vocabularies();
  $form['fieldset_vocabulary'] = array (
    '#type' => 'fieldset',
    '#title' => 'Associazione vocabolari - colonne csv',
    '#description' => 'I valori nel csv sono separati da |, es. term1 | term2 | term3 '
  );
  foreach ($vocabularies as $voc) {
    if (in_array('product', $voc->nodes)) {
      $form['fieldset_vocabulary']['voc_'.$voc->vid] = array(
        '#type'          => 'textfield',
        '#title'         => 'Colonna nel csv per '.$voc->name,
        '#multiple'      => FALSE,
        '#disabled'      => TRUE,
        '#value'         => _uc_import_get_csv_name_for($voc->vid, 'vocabulary'),
        //'#default_value' => $default_values[$voc->vid],
      );
    }
  }

  $form['fieldset_cck'] = array (
    '#type' => 'fieldset',
    '#title' => 'Associazione cck fields - colonne csv',
  );
  foreach (array('filefield', 'text', 'number_decimal', 'number_integer', 'nodereference') as $type) {
    $form['fieldset_cck']['fieldset_'.$type] = array(
      '#type'          => 'fieldset',
      '#title'         => strtoupper($type).' type CCK field(s)',
    );
  }

  $custom_fields = content_fields(NULL, 'product');
  $field_names = array();
  $field_names_form_options = array('nid' =>'NID');
  foreach ($custom_fields as $cf) {
	  $field_names[] = $cf['field_name'];
	  $field_names_form_options[$cf['field_name']] = $cf['field_name'];
  }
  
  foreach ($custom_fields as $cf) {
    if ($cf['type_name'] != 'product') {
      continue;
    }
	$variable_name_field_column = _uc_import_get_variable_name_for_field('column',$cf);
	$variable_name_field_mapping_field = _uc_import_get_variable_name_for_field('mapping_field',$cf);
	
    $form['fieldset_cck']['fieldset_'.$cf['type']]['field_'.$cf['field_name']] = array(
      '#type'          => 'textfield',
      '#title'         => 'column for '.$cf['field_name'],
      '#multiple'      => FALSE,
      '#disabled'      => TRUE,
	  '#attributes' => array('class' => 'field-node-reference'),
      '#value'         => variable_get($variable_name_field_column,_uc_import_get_csv_name_for($cf['field_name'], 'cck')),
      //'#default_value' => $default_values[$voc->vid],
    );
	// nodereference field type mapping field
	if ($cf['type'] == "nodereference") {
	    $form['fieldset_cck']['fieldset_'.$cf['type']]['field_'.$cf['field_name']."_mapping_field"] = array(
	      '#type'          => 'select',
	      '#title'         => 'mapping field',
	      '#multiple'      => FALSE,
	      '#disabled'      => FALSE,
	      '#default_value'         => variable_get($variable_name_field_mapping_field,'nid'),
		  '#attributes' => array('class' => 'field-node-reference-mapping-field'),
		  '#options' => $field_names_form_options,
	      //'#default_value' => $default_values[$voc->vid],
	    );
	}
    $form['fieldset_cck']['fieldset_'.$cf['type']]['separator_'.$cf['field_name']] = array(
         '#value' => "<hr/>",
	 );
	
	
    switch ($cf['type']) {
    case 'filefield':
      $form['fieldset_cck']['fieldset_'.$cf['type']]['field_'.$cf['field_name']]['#description'] = 'Le colonne ammesse nel csv sono del tipo ' . $cf['field_name'] . '_[n]_{path,description,title}';
      break;
    case 'text':
      $form['fieldset_cck']['fieldset_'.$cf['type']]['field_'.$cf['field_name']]['#description'] = $cf['widget']['description'];
      break;
    case 'number_decimal':
      break;
    case 'number_integer':
      break;
    case 'nodereference':
      $form['fieldset_cck']['fieldset_'.$cf['type']]['field_'.$cf['field_name']]['#description'] = "Node reference";
      break;
    default:
      continue;
      break;
    }
  }
  
  $form['submit'] = array (
    '#type'  => 'submit',
    '#value' => t('Save mapping options'),
  );

  return $form;
}
