$(document).ready(function(){
    $('#edit-file-csv').change(function(){ 
   	 	var suggested_dir = $(this).val().split('\\').pop().split('//').pop().split(".").shift().split("-").pop();
		suggested_dir = $.trim(suggested_dir)
		if (confirm("Setup import directory as "+suggested_dir+"?"))
		$("#edit-subdir-uploaded-files").val(suggested_dir)
    });
	
	$(".not-implemented").click(function(event){
		alert("Sorry, not implemented yet.")
	})
})